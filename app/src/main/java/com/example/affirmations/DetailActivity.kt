package com.example.affirmations

import android.content.ActivityNotFoundException
import com.example.affirmations.ui.theme.DetailViewModel
import android.content.Context
import android.content.Intent
import android.icu.lang.UCharacter.VerticalOrientation
import android.net.Uri
import android.os.Bundle
import android.widget.ScrollView
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.affirmations.ui.theme.AffirmationsTheme


class DetailActivity : ComponentActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AffirmationApp(intent.getIntExtra("affirmationId",0))
        }
    }
}

@Composable
fun AffirmationApp(affirmationId: Int){
    AffirmationsTheme {
        AffirmationDetail(DetailViewModel(affirmationId))
    }
}

@Composable
fun AffirmationDetail(detailViewModel: DetailViewModel){
    val uiState by detailViewModel.uiState.collectAsState()
    val context = LocalContext.current

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
                .verticalScroll(rememberScrollState())
                .background(Color.LightGray)
    ) {
        Image(
            painter = painterResource(uiState.imageResourceId),
            contentDescription = "Imatge affirmation",
            modifier = Modifier
                .scale(2.5f)
                .padding(top = 100.dp, bottom = 100.dp)
                .clip(RoundedCornerShape(100))
        )
        Text(
            text = stringResource(id = uiState.stringResourceId),
            fontFamily = FontFamily.Monospace,
            fontSize = 15.sp,
            fontWeight = FontWeight.ExtraBold,
            modifier = Modifier.padding(bottom = 40.dp, start = 30.dp, end = 30.dp)
        )
        Text(
            text = stringResource(id = uiState.descriptionId),
            modifier = Modifier
                .padding(start = 30.dp, end = 30.dp, bottom = 40.dp),
            textAlign = TextAlign.Center
        )
        Row(
            modifier = Modifier.fillMaxWidth().padding(bottom = 15.dp),
            horizontalArrangement = Arrangement.End
        ) {
            IconButton(onClick = { context.sendMail(uiState.emailTo, uiState.email) }) {
                Icon(
                    imageVector = Icons.Filled.Mail,
                    contentDescription = "Button Mail",
                    modifier = Modifier.padding(end = 30.dp)
                )
            }
            IconButton(onClick = { context.dial(uiState.phone) }) {
                Icon(
                    imageVector = Icons.Filled.Phone,
                    contentDescription = "ButtonPhone",
                    modifier = Modifier.padding(end = 30.dp)
                )
            }
        }
    }
}
fun Context.sendMail(to: String, subject: String) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(Intent.createChooser(intent, ""))
    } catch (e: ActivityNotFoundException) {

    } catch (t: Throwable) {

    }
}

fun Context.dial(phone: String) {
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    } catch (t: Throwable) {

    }
}
