/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data
import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    public fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(1,R.string.affirmation1, R.drawable.image1,R.string.description1,"111111111","manuel@gmail.com","miquel@gmail.com"),
            Affirmation(2,R.string.affirmation2, R.drawable.image2,R.string.description2,"222222222","alex@gmail.com","mireia@gmail.com"),
            Affirmation(3,R.string.affirmation3, R.drawable.image3,R.string.description3,"333333333","xavier@gmail.com","nadal@gmail.com"),
            Affirmation(4,R.string.affirmation4, R.drawable.image4,R.string.description4,"444444444","guillamon@gmail.com","robert@gmail.com"),
            Affirmation(5,R.string.affirmation5, R.drawable.image5,R.string.description5,"555555555","pol@gmail.com","montse@gmail.com"),
            Affirmation(6,R.string.affirmation6, R.drawable.image6,R.string.description6,"666666666","marc@gmail.com","carlos@gmail.com"),
            Affirmation(7,R.string.affirmation7, R.drawable.image7,R.string.description7,"777777777","noe@gmail.com","hugo@gmail.com"),
            Affirmation(8,R.string.affirmation8, R.drawable.image8,R.string.description8,"888888888","villanueva@gmail.com","maria@gmail.com"),
            Affirmation(9,R.string.affirmation9, R.drawable.image9,R.string.description9,"999999999","alejandro@gmail.com","rosa@gmail.com"),
            Affirmation(10,R.string.affirmation10, R.drawable.image10,R.string.description10,"012324423","victor@gmail.com","jaume@gmail.com"))
    }
}
